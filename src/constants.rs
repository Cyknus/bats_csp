/// Algorithm parameters.
pub const MIN_LOUDNESS: f32 = 0f32;
pub const MIN_FREQ: f32 = 0.001;
pub const MAX_FREQ: f32 = 1f32;
pub const ALPHA: f32 = 0.95f32; // Decreasing rate of loudness.
pub const GAMMA: f32 = 0.9f32; // *Something* about rate.
pub const POPULATION: usize = 20; // Number of bats in the colony.


/// Alphabet of the word set.
pub const ALPHABET: [char;26] = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

/// At most there will be this number of iterations on the main cycle.
pub const MAX_NUM_ITERATIONS: i32 = 80_000;

/// Halt condition, if this number of iterations are reached without lowering
/// the cost of a solution, the main loop will halt and the current best
/// solution will be returned.
pub const LONGEST_PERIOD: i32 = 1_000;

/// Do you want plots of the cost functions of the bats?
pub const PRINT: bool = false;

/// On what steps do you want said plots?
pub const CHARTS: [i32;18] = [1, 10, 50, 100, 500, 800, 1000, 5000, 10000, 
                                        20000, 30000,40000, 45000, 48000, 
                                        50000, 55000, 60000, 66600];

/// Nice colors for the different bats.
pub const COLORS: [&'static str; 20] = ["#000000",
                                        "#666666",
                                        "#A52A2A",
                                        "#EE2C2C",
                                        "#FF7D40",
                                        "#CDAB2D",
                                        "#EE30A7",
                                        "#8B814C",
                                        "#CECC15",
                                        "#79973F",
                                        "#488214",
                                        "#2F4F2F",
                                        "#0FDDAF",
                                        "#00F5FF",
                                        "#00BFFF",
                                        "#2B4F81",
                                        "#A020F0",
                                        "#5E2D79",
                                        "#FF030D",
                                        "#E31230"];



