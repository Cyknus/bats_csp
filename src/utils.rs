extern crate rand;
extern crate gnuplot;

use self::rand::{SeedableRng, StdRng, thread_rng, Rng};
use super::{constants, bat};
use self::gnuplot::{Figure, Color, AxesCommon, LabelOption};

/// Calculate the Hamming distance between to given strings. Both strings must
/// have the same length.
pub fn hamming_distance(center: &Vec<char>, satellite: &Vec<char>) -> i32 {
    let mut d: i32 = 0;
    for (c, s) in center.iter().zip(satellite.iter()) {
        if c != s {
            d+=1;
        }
    }
    d
}

#[test]
fn test_hamming_distance() {
    assert_eq!(1, hamming_distance(&vectorize(&"10000".to_string()), 
                                            &vectorize(&"00000".to_string())));
    assert_eq!(2, hamming_distance(&vectorize(&"10001".to_string()), 
                                            &vectorize(&"00000".to_string())));
    assert_eq!(3, hamming_distance(&vectorize(&"111".to_string()), 
                                            &vectorize(&"000".to_string())));
}

/// Generates a new seed from the integer provided by XORing it against 16 ones,
/// this assumes architecture is 64 bits.
pub fn new_seed(gen: usize) -> [usize;4] {
    let m = usize::max_value();
    let seed: &[_] = &[(12*gen) % m,(09*gen) % m,(20*gen) % m,(14*gen) % m];
    let mut rng: StdRng = rand::SeedableRng::from_seed(seed);
    let mut arr: [usize; 4] = [gen^281470684340, gen^63235, gen^429122760, gen^18442234387328430960];
    if gen % 2 == 0 {
        rng.shuffle(&mut arr);
        return arr
    }
    arr
}

#[test]
fn test_new_seed() {
    let ones: usize = 18446744073709551615; //2^64-1
    assert_eq!([18446462603027808255, 
                18446744073709486080, 
                18446744069414649855, 
                281474976710655], new_seed(ones));
}

/// Generates a new integer given a seed.
pub fn random_usize(seed: &[usize]) -> usize {
    let mut rng: StdRng = rand::SeedableRng::from_seed(seed);
    rng.gen::<usize>()
}


/// Generates n seeds from the input random.
pub fn generate_n_seeds(n: usize, random: usize) -> Vec<usize> {
    let t: usize = (random*random*random) % usize::max_value();
    recursively_generate_seeds(n, t, Vec::new())
}

fn recursively_generate_seeds(n: usize, t: usize, a: Vec<usize>) -> Vec<usize> {
    let tt = match a.last() {
        Some(val) => *val,
        None => 40737094860 as usize,
    };

    if a.len() >= n {
        a.clone()
    } else {
        recursively_generate_seeds(n, tt, trail(&a, &new_seed(t)))
    }
}

fn trail(a: &Vec<usize>, b: &[usize]) -> Vec<usize> {
    let mut res: Vec<usize> = Vec::with_capacity(a.len()+b.len());
    for e in a {
        res.push(*e);
    }
    for e in b {
        res.push(*e);
    }
    res
}

/// Take the length of the desired string, a seed and construct a new random 
/// string. It updates the seed after each letter is chosen.
pub fn random_string(length: usize, seed: &[usize]) -> Vec<char> {
    let mut new_vec: Vec<char> = Vec::with_capacity(length);
    let mut new_random: usize = random_usize(seed);

    for _ in 0..length {
        let new_seed: &[_] = &new_seed(new_random);
        let i: usize = new_random % constants::ALPHABET.len();
        new_vec.push(constants::ALPHABET[i]);
        new_random = random_usize(new_seed);
    }
    new_vec
}

/// Take the length and number of desired strings and a random number as a seed
/// to keep generating random strings.
pub fn random_strings(length: usize, n: usize, random: usize) {
    let mut n_s = new_seed(random);
    let mut rng = thread_rng();
    let mut i = 0;
    while i < n {
        println!("{}", random_string(length, &n_s).into_iter().collect::<String>());
        let n_r = random_usize(&n_s);
        n_s = new_seed(n_r);
        rng.shuffle(&mut n_s);
        i+=1;
    }
}

/// Generates a random float of 32 bits precision with a uniform distribution in
/// [0,1] range.
pub fn beta(seed: &[usize]) -> f32 {
    let mut rng: StdRng = SeedableRng::from_seed(seed);
    rng.gen::<f32>()
}

/// Takes a string and forms a vector with the same chars in the same order, 
/// this is because strings in Rust can not be accessed by index, goddammit.
/// Linear time, of course.
pub fn vectorize(s: &String) -> Vec<char> {
    let mut vec: Vec<char> = Vec::with_capacity(s.len());
    for c in s.chars() {
        vec.push(c)
    }
    vec
}

/// Let's take it one step beyond, lets transform a vector of strings into a 
/// vector of vectors of chars. Same as vectorize, same chars, same order.
pub fn vectorize_vector(vector: &Vec<&str>) -> Vec<Vec<char>> {
    let mut vec: Vec<Vec<char>> = Vec::with_capacity(vector.len());
    for s in vector {
        vec.push(vectorize(&s.to_string()))
    }
    vec
}

/// Given an n, a string and a seed, change n letters from the given string,
/// randomly. I keep track of the indices changed due to the fact that with 
/// short length strings the random numbers might be repeated (because modulo),
/// and we want an exact, n, hamming distance between the input and the output.
/// Returns a new vector of chars.
pub fn change_n(n: i32, loc: &Vec<char>, seed: &[usize]) -> Vec<char>{
    let mut new_random: usize = random_usize(seed);
    let alphalen: usize = constants::ALPHABET.len();
    let loc_len = loc.len();
    let mut chch = n; // CHanged CHaracters, bc n is immutable.
    let mut changes: Vec<usize> = Vec::with_capacity(n as usize);
    let mut new_loc: Vec<char> = loc.clone();

    while chch > 0 {
        let i: usize = new_random % loc_len;
        if new_loc[i] != constants::ALPHABET[(new_random>>1) % alphalen] &&
            !changes.contains(&i) { 
            new_loc[i] = constants::ALPHABET[(new_random>>1) % alphalen];
            chch-=1;
            changes.push(i);
        }
        let new_seed: &[_] = &new_seed(new_random);
        new_random = random_usize(&new_seed);
    }
    new_loc
}

#[test]
fn test_change_n() {
    let a: Vec<char> = vec!['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    let b: Vec<char> = change_n(3, &a, &[22221,3232345,5470,30429,23000]);
    assert!(a!=b);
    assert_eq!(3, hamming_distance(&a, &b));
}

// Same as above but as the input is mutable, does not return anything new.
pub fn change_n_in_place(n: i32, loc: &mut Vec<char>, seed: &[usize]) {
    let mut new_random: usize = random_usize(seed);
    let alphalen: usize = constants::ALPHABET.len();
    let loc_len = loc.len();
    let mut chch = n;
    let mut changes: Vec<usize> = Vec::with_capacity(n as usize);

    while chch > 0 {
        let i: usize = new_random % loc_len;
        if loc[i] == constants::ALPHABET[(new_random>>1) % alphalen] ||
            changes.contains(&i) {
        } else {
            loc[i] = constants::ALPHABET[(new_random>>1) % alphalen];
            chch-=1;
            changes.push(i);
        }
        let new_seed: &[_] = &new_seed(new_random);
        new_random = random_usize(&new_seed);
    }
}

/// Plots Hamming distance from every bat to every word. Sorted distances.
pub fn plot(colony: &Vec<bat::Bat>, s_vector: &Vec<Vec<char>>, name: String) {
    
    let mut ys = Vec::with_capacity(s_vector.len());
    let mut yss = Vec::with_capacity(s_vector.len());
    
    for b in colony {
        ys.push(distances_unsorted(&b.location, &s_vector));
        yss.push(distances(&b.location, &s_vector));
    }

    gnuplot_it(&ys, false, &name);
    gnuplot_it(&yss, true, &name);
}

/// Calls gnuplot to... well, plot. Has both options for sorted and unsorted
/// arrays (the only difference is the name of the output file)
fn gnuplot_it(ys: &Vec<Vec<i32>>, sorted: bool, name: &str) {
    let x: Vec<_> = (0..ys[0].len()+1).collect(); 
    let mut fg = Figure::new();
    let path: String;

    if sorted {
        path = "src/img/sorted/iter_".to_string() + &name.to_string();
    } else {
        path = "src/img/unsorted/iter_u_".to_string() + &name.to_string();
    }

    fg.set_terminal("png", &path);

    {
        let axis2d = fg.axes2d().set_x_label(&"Word",&[])
                .set_y_label(&"Hamming Distance", &[LabelOption::Rotate(90.0)])
                .set_title(&"Bats' locations", &[]);

        for (i,y) in ys.iter().enumerate() { 
            axis2d.lines(&x, y.as_slice(), &[Color(constants::COLORS[i])]);
        }
    }

    fg.show();
}

/// Plots just one location. Both sorted and usorted.
pub fn single_plot(location: &Vec<char>, s_vector: &Vec<Vec<char>>, name: String) {
    let x: Vec<_> = (0..s_vector.len()).collect();
    let mut ys = Vec::with_capacity(2);
    ys.push(distances_unsorted(location, s_vector));
    ys.push(distances(location, s_vector));

    let mut fg = Figure::new();
    let path = "src/img/single/s_iter_".to_string() + &name.to_string();
    fg.set_terminal("png", &path);

    {
        let axis2d = fg.axes2d().set_x_label(&"Word",&[])
                .set_y_label(&"Hamming Distance", &[LabelOption::Rotate(90.0)])
                .set_title(&"Bats' locations", &[]);

        for (i,y) in ys.iter().enumerate() { 
            axis2d.lines(&x, y.as_slice(), &[Color(constants::COLORS[i+5])]);
        }
    }

    fg.show();
}

/// Auxiliar function to plot. Calculate de distances from location1 to every
/// location in the array, there is an entry for every word.
/// Due to the known bound of the distances, it sorts them while calculating 
/// them.
fn distances(location1: &Vec<char>, location_arr: &Vec<Vec<char>>) -> Vec<i32> {
    let mut distances = vec![0;location1.len()+1];
    for l in location_arr {
        distances[hamming_distance(location1, &l) as usize]+=1
    }
    build_vector_with(&distances)
}

#[test]
fn test_distances() {
    assert_eq!(vec![0,0,0], distances(&vec!['a','a','a'],
        &vec![vec!['a','a','a'],vec!['a','a','a'],vec!['a','a','a']]));
    assert_eq!(vec![0,1,1], distances(&vec!['a','a','a'],
        &vec![vec!['a','a','a'],vec!['a','a','b'],vec!['a','a','b']]));
    assert_eq!(vec![0,1,2], distances(&vec!['a','a','a'],
        &vec![vec!['a','a','a'],vec!['a','a','b'],vec!['a','b','b']]));
    assert_eq!(vec![2,2,3], distances(&vec!['a','a','a'],
        &vec![vec!['c','b','a'],vec!['g','a','b'],vec!['b','b','b']]));
}

/// Auxiliar function to distances(). Takes an array that contains the repetions
/// of every distance and repeats them to fill the arrray with an entry for
/// every word.
fn build_vector_with(spec: &Vec<i32>) -> Vec<i32> {
    let mut res = Vec::new();
    for (i, v) in spec.iter().enumerate() {
        if *v != 0 {
            let mut this = vec![i as i32;*v as usize];
            res.append(&mut this);
        }
    }
    res
}

#[test]
fn test_build_vector_with() {
    assert_eq!(vec![0,0,0], build_vector_with(&vec![3]));
    assert_eq!(vec![0,0,0,1,2,2,3,3,3,3,3], build_vector_with(&vec![3,1,2,5]));
    assert_eq!(vec![0,0,0,2,2,3,3,3,3,3], build_vector_with(&vec![3,0,2,5]));
}

/// Auxiliar function to plot. Gets the distances but do not sorts them.
fn distances_unsorted(location1: &Vec<char>, location_arr: &Vec<Vec<char>>) -> Vec<i32> {
    let mut distances = Vec::with_capacity(location_arr.len());
    for l in location_arr {
        distances.push(hamming_distance(location1, &l));
    }
    distances
}


// Function to print with certain format.
pub fn print_pretty(t: (String, f32)) {
    println!("Closest String I came up with is: {}", t.0);
    println!("The cost of said string is: {}", t.1);
}