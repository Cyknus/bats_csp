use super::{constants, utils};


#[derive(Debug)]
pub struct Bat {
    id: i32,
    freq: f32,
    initial_rate: f32,
    pub current_rate: f32,
    velocity: f32,
    pub loudness: f32,
    pub location: Vec<char>,
    pub quality: f32,
    random: usize,
    t: i32,
}

pub trait Behavior {

    fn adjust_frequency(&mut self);

    fn update_velocity(&mut self, &Vec<char>);
    
    fn update_seed(&mut self) -> [usize;4];

    fn fly_locally(&mut self, f32, &Vec<char>);

    fn fly_randomly(&mut self);

    fn fly_truly_randomly(&mut self);

    fn increase_rate(&mut self);

    fn reduce_loudness(&mut self);

    fn get_old(&mut self);
}


/// Constructs a new Bat with given specs.
pub fn new(id: i32, freq: f32, 
        initial_rate: f32, 
        current_rate: f32, 
        velocity: f32,
        loudness: f32,
        location: Vec<char>,
        quality: f32,
        random: usize,
        t: i32) -> Bat {
    Bat {id: id, freq: freq, 
            initial_rate: initial_rate,
            current_rate: current_rate,
            velocity: velocity,
            loudness: loudness,
            location: location,
            quality: quality,
            random: random,
            t: t }
}

/// Initializes bat colony.
pub fn initialize_colony(len: f32, seeds: &[usize]) -> Vec<Bat> {

    let mut colony: Vec<Bat> = Vec::with_capacity(constants::POPULATION);

    for i in 0..constants::POPULATION {
        let seed: &[_] = &utils::new_seed(seeds[i]);
        let freq: f32 = utils::beta(seed);
        let location: Vec<char> = utils::random_string(len as usize, seed);
        let random: usize = utils::random_usize(seed);
        let b: Bat = new(i as i32, freq, 0.01f32, 0.01f32, len, len, location, 0f32, random, 0);
        colony.push(b)
    }
    colony
}

// O(n)
pub fn get_best_location(colony: &Vec<Bat>) -> Vec<char> {
    let mut best_location: i32 = 0;
    let mut best_quality: f32 = colony[0].quality;
    for bat in colony {
        if bat.quality > best_quality {
            best_location = bat.id;
            best_quality = bat.quality
        }
    }
    colony[best_location as usize].location.clone()
}

pub fn avg_loudness(colony: &Vec<Bat>) -> f32 {
    let mut sum: f32 = 0f32;
    for b in colony {
        sum += b.loudness
    }
    sum/colony.len() as f32
}

impl Behavior for Bat {

    /// Folowing the paper instructions. Equation (2).
    fn adjust_frequency(&mut self) {
        let seed: &[_] = &(self.update_seed());
        self.freq = constants::MIN_FREQ + 
        (constants::MAX_FREQ - constants::MIN_FREQ) * utils::beta(seed);
    }

    // Updates random attribute of Bat and returns a new seed.
    fn update_seed(&mut self) -> [usize;4] {
        let new_seed = utils::new_seed(self.random);
        self.random = utils::random_usize(&new_seed);
        new_seed
    }

    /// This is tricky, still experimental. 
    /// Folowing the paper instructions. Equation (3). 
    fn update_velocity(&mut self, global_optimum: &Vec<char>) {
        let lambda: f32 = 
                utils::hamming_distance(global_optimum, &self.location) as f32;
        let new_v: usize = (self.velocity + lambda * self.freq).ceil() as usize;
        self.velocity = (new_v % self.location.len()) as f32;
    }
    
    /// This is not yet implemented bc i still have not decided what is local.
    /// Maybe what the paper says (using loudness), maybe a permutation of 2 
    /// letters.
    fn fly_locally(&mut self, avg_loudness: f32, best: &Vec<char>) {
        let seed: &[_] = &self.update_seed();
        let epsilon: f32 = utils::beta(seed);
        let n: i32 = (epsilon * avg_loudness).ceil() as i32;
        self.location = utils::change_n(n, best, seed);
    }

    /// Randomly moves to another location with current velocity.
    fn fly_randomly(&mut self) {
        let seed: &[_] = &self.update_seed();
        utils::change_n_in_place(self.velocity as i32, &mut self.location, seed)
    }

    /// Randomly creates a new string (location).
    fn fly_truly_randomly(&mut self) {
        let seed: &[_] = &(self.update_seed());
        self.location = utils::random_string(self.location.len(), seed);
    }

    /// Folowing the paper instructions. Equation (6).
    fn increase_rate(&mut self) {
        self.current_rate = self.initial_rate * 
                                (1.0 - (-self.t as f32 * constants::GAMMA).exp());
    }

    /// Folowing the paper instructions. Equation (7).
    fn reduce_loudness(&mut self) {
        self.loudness *= constants::ALPHA;
    }

    /// Need to keep control of the step we are at because increase_rate().
    fn get_old(&mut self) {
        self.t+=1
    }
}