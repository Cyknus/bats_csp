/// This software is licensed under the "Anyone But Richard M Stallman"
/// (ABRMS) license, described below. No other licenses may apply.


/// --------------------------------------------
/// The "Anyone But Richard M Stallman" license
/// --------------------------------------------

/// Do anything you want with this program, with the exceptions listed
/// below under "EXCEPTIONS".

/// THIS SOFTWARE IS PROVIDED "AS IS" WITH NO WARRANTY OF ANY KIND.

/// In the unlikely event that you happen to make a zillion bucks off of
/// this, then good for you; consider buying a homeless person a meal.


/// EXCEPTIONS
/// ----------

/// Richard M Stallman (the guy behind GNU, etc.) may not make use of or
/// redistribute this program or any of its derivatives.


extern crate bats_csp;
extern crate pbr;

use bats_csp::{algorithm, utils, constants};
use std::env;
//use pbr::ProgressBar;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::error::Error;

fn main() {

    // Reads the initial random value to initialize seeds for each bat.
    let random: usize = match env::args().nth(1) {
        Some(val) => val.parse::<usize>().unwrap(),
        None => panic!("You must provide a random integer to initialize seed."),
    };

    // Reads the path to the file containing the set of strings.
    let arg_path: String = match env::args().nth(2) {
        Some(n) => n,
        None => panic!("You must provide a file with the strings."),
    };

    // Converts string from argument to Path structure.
    let path = Path::new(&arg_path);
    let mut file = match File::open(&path) {
        Err(why) => panic!("Error with path: {}", why.description()),
        Ok(file) => file,
    };

    let mut input_str = String::new();

    // Vector with strings.
    let input_vec: Vec<&str> = match file.read_to_string(&mut input_str) {
        Err(why) => panic!("Error reading file: {}", why.description()),
        Ok(_)    => input_str.split(',')
                             .map(|s| s.trim())
                             .collect::<Vec<&str>>(), 
    };

    println!("Initial random: {}", random);
    if input_vec.len() == 1 {panic!("Just one string? really?");}


    //utils::random_strings(10 as usize, 5000 as usize, 11289987773846327 as usize);
    //ejemplo1();
    run(random, &input_vec);
    //lowest(input_vec);

}


/// Test function to find by brute force the minimum cost that a string can have
// fn lowest(input_vec: &Vec<&str>) {

//     let test: Vec<Vec<char>> = utils::vectorize_vector(input_vec);
//     let mut pb = ProgressBar::new(308915776 as u64); // 26^6
//     pb.format("|-C·|");
//     let mut min: f32 = 9999.0;
//     for a in constants::ALPHABET.iter() {
//         for b in constants::ALPHABET.iter() {
//             for c in constants::ALPHABET.iter() {
//                 for d in constants::ALPHABET.iter() {
//                     for e in constants::ALPHABET.iter() {
//                         for f in constants::ALPHABET.iter() {
//                             pb.inc();
//                             let v = vec![*a,*b,*c,*d,*e,*f];
//                             let cost = algorithm::objective_function(&v, &test);
//                             if cost < min { min = cost }
//                         }
//                     }
//                 }
//             }
//         }
//     }
//     pb.finish_print("done");
//     println!("{}", min);
// }


// fn ejemplo1() {
//     let ss: Vec<&str> = vec!["uui", "skk", "pwo", "din"];
//     let ee: &[_] = &[323,2323,323,323,6265426542,654,6,5,464,5,64,665,65,546546,546,54,654,654,6,54];
//     let s: (String, f32) = algorithm::algorithm(&utils::vectorize_vector(&ss), ee);
//     println!("{:?}", s);
// }

fn run(random: usize, input_vec: &Vec<&str>) {
    let ee = utils::generate_n_seeds(constants::POPULATION, random);
    let s: (String, f32) = algorithm::algorithm(&utils::vectorize_vector(&input_vec), ee.as_slice());
    utils::print_pretty(s);
}
