extern crate pbr;

use super::{utils, constants, bat};
use bat::Behavior;
use self::pbr::ProgressBar;

// Exact* copy of the algorithm in "A New Metaheuristic Bat-Inspired Algorithm"
pub fn algorithm(s_vector: &Vec<Vec<char>>, seeds: &[usize]) -> (String, f32) {
    // This is just the progress bar.
    let mut pb = ProgressBar::new(constants::MAX_NUM_ITERATIONS as u64);

    let mut colony: Vec<bat::Bat> = bat::initialize_colony(s_vector[0].len() as f32, seeds);
    update_quality(&mut colony, s_vector);
    let mut best_location: Vec<char> = bat::get_best_location(&colony);

    pb.format("|-C·|");
    pb.show_tick = true;

    let mut t: i32 = 0;
    let mut previous_best: f32 = 0.0;
    let mut longest_period = 0;
    while t < constants::MAX_NUM_ITERATIONS {
        pb.inc();
        let mut f_star: f32 = 0.0;
        let avg_loudness = bat::avg_loudness(&colony);
        for b in colony.iter_mut() {
            if utils::beta(&b.update_seed()) > b.current_rate {
                b.fly_locally(avg_loudness, &best_location);
                best_location = bestie(&best_location, &b.location, s_vector);
            }

            b.fly_randomly();

            let f_x = objective_function(&b.location, s_vector);
            f_star = objective_function(&best_location, s_vector);
            if utils::beta(&b.update_seed()) < b.loudness && f_x < f_star{
                best_location = b.location.clone();
                b.increase_rate();
                b.reduce_loudness();
            }
        }

        if constants::CHARTS.contains(&t) && constants::PRINT {
            utils::plot(&colony, &s_vector, t.to_string());
            utils::single_plot(&best_location, &s_vector, t.to_string());
        }

        t+=1;

        if previous_best == f_star { 
            longest_period+=1;
        } else {
            previous_best = f_star;
            longest_period = 0;
        }

        if longest_period >= constants::LONGEST_PERIOD { break; }

    }
    pb.finish_print("done");
    let best_cost: f32 = objective_function(&best_location, s_vector);
    (best_location.into_iter().collect(), best_cost) 
}

/// This is the one we want to find the minimum value.
/// Returns the Hamming distance divided by the length of the strings, divided
/// by the number of strings.
pub fn objective_function(closest: &Vec<char>, s_vector: &Vec<Vec<char>>) -> f32 {
    let mut sum: i32 = 0;
    for s in s_vector {
        sum += utils::hamming_distance(closest, &s);
    }
    (sum as f32 / closest.len() as f32) / s_vector.len() as f32
}

#[test]
fn test_objective_function() {
    let mut test_vec: Vec<Vec<char>> = Vec::new();
    for s in ["aabab", "aabba", "aaaaa", "ababa", "babac"].iter() {
        test_vec.push(utils::vectorize(&s.to_string()));
    }
    assert_eq!(0.8, objective_function(&utils::vectorize(&"acccc".to_string()), &test_vec));
}

/// Ok, i put this one here because i did not want to pass the strings vector to
/// a method in the bat module AND then call the objective function.
/// So it takes a colony and assings a quality value to each location.
/// This is done with the objective_function, so the lower, the better.
fn update_quality(colony: &mut Vec<bat::Bat>, s_vector: &Vec<Vec<char>>) {
    for b in colony {
        b.quality = objective_function(&b.location, s_vector);
    }
}

/// Returns a clone of the best location determined by the objective_function.
fn bestie(location_1: &Vec<char>, location_2: &Vec<char>, colony: &Vec<Vec<char>>) -> Vec<char> {
    let quality_1 = objective_function(location_1, colony);
    let quality_2 = objective_function(location_2, colony);
    if quality_1 < quality_2 {
        location_1.clone()
    } else {
        location_2.clone()
    }
}