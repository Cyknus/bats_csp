Welcome to the Batcave
===================

First things first:

>This software is licensed under the "Anyone But Richard M Stallman"
>(ABRMS) license, described below. No other licenses may apply.

--------------------------------------------
>The "Anyone But Richard M Stallman" license
--------------------------------------------

>Do anything you want with this program, with the exceptions listed
>below under "EXCEPTIONS".

>THIS SOFTWARE IS PROVIDED "AS IS" WITH NO WARRANTY OF ANY KIND.

>In the unlikely event that you happen to make a zillion bucks off of
>this, then good for you; consider buying a homeless person a meal.


>EXCEPTIONS
----------

>Richard M Stallman (the guy behind GNU, etc.) may not make use of or
>redistribute this program or any of its derivatives.

----------

The code here is based on the algorithm proposed by Xin-She Yang [^XSY]. 

----------


### Usage

After cloning the repository:

`$ cd bats_csp `

`$ cargo run --release <seed> <path/to/input/file> `

Where `<seed>` is an integer and the input file is plain text with same-length strings separated by a comma.

There are two example files that can be used by typing:

`$ cargo run --release $RANDOM src/input/700_30 `

or

`$ cargo run --release $RANDOM src/input/1500_15 `

You can tweak some options on the src/constans.rs file.

------------

### Graphs

Graphs are in src/img

--------------

  [^XSY]: [X. S. Yang, “A new metaheuristic bat-inspired algorithm”, in Proc. Nature Inspired Cooperative Strategies for Optimization (NISCO 2010), Springer Berlin, vol. 284, 2010, pp. 65–74.](https://arxiv.org/pdf/1004.4170.pdf)


